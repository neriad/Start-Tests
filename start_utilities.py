import os
import time
from uiautomator import Device

device_id = os.environ.get("device_running_id")
device_obj = Device(device_id)


def if_questionary_skip():
    print("in if_questionary_skip")
    try:
        time.sleep(2)
        device_obj.press.back()
        f = device_obj(resourceId="com.celltick.lockscreen:id/questionnaire_view_pager")
        if len(f) > 0:
            return skip_questionary()
        else:

            device_obj.press.back()
            time.sleep(2)
            f = device_obj(resourceId="com.celltick.lockscreen:id/questionnaire_view_pager")
            if len(f) > 0:
                return skip_questionary()
            print("no questionary")
    except Exception as e:
        print(e)
        return False


def click_confirmation_button():
    device_obj(resourceId="com.celltick.lockscreen:id/confirmation_button").click()


def skip_questionary():
    print("in skip_questionary")
    try:
        answer_questionary("Male")
        answer_questionary("Under 24")
        answer_questionary("Travel")
        click_confirmation_button()
        os.environ["RAV_QUESTIONARY_ANSWERED"] = "true"
        return True
    except Exception as e:
        print(e)
        device_obj(text="No, thanks").click()
        device_obj.press.home()
        return False


def answer_questionary(label_name):
    print("in answer questionary: ",label_name)
    try:
        time.sleep(1)
        device_obj(text=label_name).click()
    except Exception as e:
        print(e)
        device_obj(resourceId="com.celltick.lockscreen:id/dismiss_questionnaire").click()