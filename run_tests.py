import os
import time
import subprocess
from datetime import datetime
from pathlib import Path
from uiautomator import Adb

import log_utilities
from start_functions_2 import send_mail
#3
'''
uncomment in case you need to delete directory with sudo 
sudoPassword = 'ravtech!'
command = "sudo rm {}".format(image_name)
p = os.system('echo %s|sudo -S %s' % (sudoPassword, command))
'''

on_server = True
on_jenkins = True
behave = True

os.environ["RAV_SHARE_APPS_LIST_DISMISSED"] = "false"
os.environ["RAV_QUESTIONARY_ANSWERED"] = "false"

#print("Nightly: BUILD ",os.environ.get("RAV_NIGHTLY_APK"))

if behave:
    os.environ["RAV_BEHAVE"] = "TRUE"

on_server = False
# chmod -R 777 /var/lib/jenkins/workspace/`ls -1tr /var/lib/jenkins/workspace/ | tail -1`
if on_server:
    Home_Directory = Path.home() / "workspace"

else:
    os.environ["RAV_BEHAVE"] = "FALSE"
#os.environ["RAV_IS_DEBUG"] = "TRUE"

def get_test_suit_behave(test_suit):
    arr = []
    for test in test_suit:
        tmp = test.strip()
        tmp = "\'" + test + "\'"
        arr.append(tmp)
    return arr

print("is in debug mode: ",os.environ.get("RAV_IS_DEBUG"))
disable_configure = True
os.environ["RAV_ON_SERVER"] = str(on_server)

workspace_ = Path("/") / "home" / "share" / "workspace"  # TODO: Move to config file
pictures_ = Path.home() / "Pictures"

Home_Directory = workspace_ if on_server else pictures_
os.environ['HOME_DIRECTORY'] = str(Home_Directory)
home_dir = os.environ.get('HOME_DIRECTORY')
build_number = os.environ.get('BUILD_NUMBER')
Job_Name = os.environ.get('JOB_NAME')

change_xml_timestamp = "touch -am /var/lib/jenkins/workspace/Start-Automation/astart_report.xml"
#subprocess.check_output(change_xml_timestamp.split())

print("JENKINS BUILD NUMBER  ",build_number)
os.environ["RAVTECH_NOW"] = "None"
os.environ['test_failures_path'] = "None"
os.environ['RAV_BACKGROUND_IMG'] = str(Path(Home_Directory) / "html_images/backgr_1.jpg")
#print("os.environ['RAV_BACKGROUND_IMG'] = ",os.environ.get("RAV_BACKGROUND_IMG"))
print("workspace path: ", home_dir)

mail_list = os.environ.get('MAIL_RECIPIENTS')
print("mail_list ",mail_list)

adb = Adb()
devices_dict = adb.devices()

print("All connected devices", devices_dict)

#print("devices_1: ", os.environ.get('TEST_DEVICES'))
devices = os.environ.get('TEST_DEVICES').split(',')
print("devices: ", devices)
#devices_phone_numbers = os.environ.get("RAV_PHONE_NUMBERS")
os.environ['RAV_SERVER_PHONE_NUMBER'] = "0508481987"
os.environ['RAV_SERVER_PHONE_ID'] = "ada140cf"
nightly = os.environ.get('RAV_NIGHTLY')
print("nightly", nightly)
test_suit = os.environ.get('TESTS').split(",")
#44
#devices = ["J1 : 4200c844e4262300"]#["Alcatel 5045A:Y5CQMBNJNNNBEUN7","Huawei P9:MWS0216622001532","alcatel5010G Pixi4 5:ONGIZTTW8H6SJFWW","TCL 5010G:HEHMR4Q84DDY8PNN","Telcel-L950: ada140cf","alcatel5010G Pixi4 5:ONGIZTTW8H6SJFWW","J1 : 4200c844e4262300","BLU 4G LTE d3116:D311A1ZR5B090118","Google pixel 7:HT6C90200097","ZTE Blade L5:NNW4JJ4T4LR4G66H"]#
#devices = ["TCL 5010G:HEHMR4Q84DDY8PNN","alcatel5010G Pixi4 5:ONGIZTTW8H6SJFWW","J1 : 4200c844e4262300","Huawei P9:MWS0216622001532","TCL 5010G:HEHMR4Q84DDY8PNN","Nexus 5x:00861e9da2554dd9","Telcel-L950: ada140cf","BLU 4G LTE d3116:D311A1ZR5B090118"]
if not on_jenkins:
    # if not disable_recording or disable_configure:
    #     os.environ['DISABLE_TEST_RECORDING'] = "0"
    #
    # if not devices_phone_numbers and not on_server or disable_configure:
    #     os.environ['RAV_PHONE_NUMBERS'] = "ce091609616c472903:0502489111,c995d155:0504966999"

    if not devices or disable_configure:
        devices = ["telcel L950:ada140cf","Samsung small:0bf2d57b", "LG tikun:083809860a8b5482",
                   "Pixie4:ONGIZTTW8H6SJFWW","Samsung Not 3 RAVTECH:b6afb4da","Huawei:MWS0216622001532","Blade L5:NNW4JJ4T4LR4G66H"]

    if not mail_list or disable_configure:
        mail_list = "mordechai@ravtech.co.il"

    # if not disable_recording or disable_configure:
    #     disable_recording = ""

    if not test_suit or disable_configure:
        # test_suit = ["test_ring_add_shortcut_via_plus"]
        test_suit = [
            "test_advanced_settings_disable_show_hints"
        ]

print("devices ", devices)
print("Devices from env variable:  ", devices)

if not on_server:
    path_to_features = "/home/user/Start-Tests/features"
else:
    path_to_features = "/home/itzik/PycharmProjects/Start-Tests/features"

print("DISABLE_TEST_RECORDING env variable:", os.environ.get('DISABLE_TEST_RECORDING'))
# TODO Change it: DISABLE_TEST_RECORDING
os.environ['DISABLE_TEST_RECORDING'] = "1"
print("test_suit: {}".format(test_suit))

# cmd_str = "py.test helper_for_ring_and_phone_tests.py -s -k '{} " + "or {}  " * (len(test_suit) - 1) + "'" \
#           + " --junitxml=astart_report.xml"
# cmd = cmd_str.format(*test_suit)

test_suit_behave = get_test_suit_behave(test_suit)
print("length ",len(test_suit_behave))
cmd_str_behave = "python3 -m behave  {} ".format(path_to_features)
print("cmd_str_behave ",cmd_str_behave)
cmd_str_behave = cmd_str_behave + " -n {}" * (len(test_suit_behave))
print("cmd_str_behave ",cmd_str_behave)
cmd_str_behave = cmd_str_behave.format(*test_suit_behave)
print("cmd_str_behave ",cmd_str_behave)
#todo: delete when running with pytest
#cmd = "behave  features/advanced_settings.feature"

#print("cmd ", cmd)
failures_folder_path = Home_Directory / "failures_images"
try:
    failures_folder_path.mkdir(parents=True, exist_ok=True)
except FileExistsError:
    print("failure_images already exists")

now = datetime.now()
now = str(now.replace(microsecond=0))
os.environ["RAVTECH_NOW"] = now
test_failures_path = failures_folder_path / now
test_failures_path = Path(str(test_failures_path).replace(" ", "_").replace(":","_").replace("-","_"))
print("replaced failures dolder path {}".format(test_failures_path))
if not os.path.exists(str(test_failures_path)):
    try:
        os.makedirs(str(test_failures_path))
    except FileExistsError:
        print("test_failures_path already exists")
    # give_permissions(test_failures_path)
    os.environ['test_failures_path'] = str(test_failures_path)

log_utilities.print_tests_suit_header(now,devices,devices_dict,
                                      test_suit,build_number,Job_Name)
for device_id in (devices or []):
    print("device:", device_id)
    serial = device_id.split(":")[1].strip()
    device_name = device_id.split(":")[0].strip()
    device_name.replace("-","_").replace(" ","_")
    print("device name: ", device_name)
    print("serial", serial)
    if serial in devices_dict:
 #       print("%" * 70)

        print("yes device appear in device dict and connect to linux server")
        os.environ['device_running_id'] = serial
        os.environ['device_running_name'] = device_name
        device_names = serial + "_" + device_name
        device_folder_name = device_names.replace(" ", "_")
        os.environ["RAV_DEVICE_FOLDER"] = str(Path(home_dir)/ "devices" / device_folder_name)
        failures_folder_path = test_failures_path / device_folder_name
        failures_folder_path.mkdir(parents=True, exist_ok=True)
        # give_permissions(failures_folder_path)
        os.environ['DEVICE_FAILURE_FOLDER'] = str(failures_folder_path)
        print(" str(failures_folder_path)",  str(failures_folder_path))
        videos_path = failures_folder_path / "videos"
        print("videos_path ", videos_path)
        try:
            os.makedirs(str(videos_path))
        except FileExistsError:
            print("ERROR: File already exists")
        #print("CMD ", cmd)
        #cmd2 = "python3 -m behave features/widgets.feature"
        #cmd2 = "python3 -m behave  {}".format(path_to_features)
        cmd2 = "python3 -m behave  {} -n {} ".format(path_to_features,"'contact'")
        #cmd2 = "behave -i" + path_to_features + "/'stress tests.feature'"#4
        print("666")
        if behave:
            print(*"777")
        cmd = cmd_str_behave
        subprocess.Popen(cmd, shell=True).wait()

html_log = log_utilities.HtmlLog()
html_log.write_log_bottom_html()
time.sleep(1)

mail_list = os.environ.get('MAIL_RECIPIENTS')
print("mail_list ",mail_list)

send_mail(mail_list, "log file and all failures links")
