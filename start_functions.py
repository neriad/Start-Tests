import inspect
import os
import threading
from functools import partial
from time import sleep, time
from pathlib import Path
from subprocess import check_output
import cv2
import xml.etree.ElementTree as ET
from difflib import SequenceMatcher as SM
import log_utilities
import pytesseract
# import uiautomator_canvas as canvas
from PIL import Image
from uiautomator import JsonRPCError
from uiautomator_canvas import PathOrStr, get_bounds, is_black_theme

import config
import start_functions_2 as stf2
from settings import device_obj, device_id, device_name, device_folder_name, adb_functions, canvas

log_html = log_utilities.HtmlLog()


def catch_any_error_and_return_false(any_func):
    def wrapper(*args, **kwargs):
        try:
            print("in try")
            return any_func(*args, **kwargs)
        except Exception as e:
            print("erro catched")
            print("****ERROR****")
            print(e)
            print("*"*13)
            return False
    return wrapper




class StartFunctions:
    def __init__(self):
        os.environ["DEVICE_STATUS"] = "1"
        self.device_obj = device_obj
        self.device = device_id
        self.device_name = device_name
        self.adb = adb_functions
        self.canvas = canvas
        print("self.device:", self.device)
        self.home_path = Path(os.environ["HOME_DIRECTORY"])
        self.device_folder = Path(os.environ.get("RAV_DEVICE_FOLDER"))#self.home_path /
        # device_folder_name
        self.home_screen_xml = str(self.device_folder / "home_screen.xml")
        self.create_folder_for_images(self.device_folder)
        self.index = None
        self.number_of_favorite_shortcut = None
        self.last_shortcut_status = None
        self.lock_point = None
        self.screen_width, self.screen_height = self.adb.get_physical_device_screen_size()
        print("self.screen_width, self.screen_height:", self.screen_width, self.screen_height)
        self.menu_point = (int(self.screen_width - self.screen_width/15),
                           int(self.screen_height/2))
        self.favorite_point = None
        self.plus_point = None
        self.plus_point_last = None
        self.chrome_point = None
        self.search_hub = None
        self.contact_hub = None
        self.delete_point = None
        self.message_hub_point = None
        self.mail_point = None
        self.sms_point = None
        self.multimedia_hub_point = None
        self.camera_point = None
        self.gallery_point = None
        self.unlock_point = None
        self.settings_point = None


    def __call__(self, *args, **kwargs):
        return self


    # noinspection PyArgumentList
    def crop_all_hub_icons(self, place):
        self.press_on_lock_and_screenshot(place)
        # self.start_thread(long_swipe_on_lock)
        # sleep(2)
        # self.start_thread(screenshot)
        # self.join_threads()
        object_coordinates_arr, images_details = self.canvas.identify_all_hubs_and_crop_to_images(
            self.screen_height,self.screen_width)
        print("number of hubs:", len(object_coordinates_arr), object_coordinates_arr)
        for i in range(len(object_coordinates_arr)):
            self.press_on_hub_and_screenshot(i, object_coordinates_arr[i], images_details[i])

    def press_on_lock_and_screenshot(self, place):
        print("in press_on_lock_and_screenshot")
        screenshot = partial(self.adb.screenshot_by_adb,
                             image_name=self.device_folder / "select_options.png", place=place,
                             wait=20)
        print("5555555555 self.lock_point: ", self.lock_point, "self.screen_height: ",
              self.screen_height)
        if not self.lock_point:
            lp,tmp = canvas.identify_lock_object()
            self.lock_point = lp[0]
            if not lp:
                self.lock_point = self.guess_lock_point()
        long_swipe_on_lock = partial(stf2.long_swipe_on_point_from_point,
                    self.lock_point, (self.lock_point[0], self.lock_point[1] - int(self.screen_height * 0.03)), 95)
        self.run_in_parallel(long_swipe_on_lock, screenshot)

    def run_in_parallel(*fns):
        threads = []
        for fn in fns:
            print(fn)
            t = threading.Thread(target=fn)
            threads.append(t)
            t.start()

        for t in threads:
            t.join()


    @staticmethod
    def create_folder_for_images(folder_name_to_be_created: PathOrStr) -> None:
        folder = Path(folder_name_to_be_created)
        folder.mkdir(exist_ok=True, parents=True)

    def get_lock_coordinates(self) -> tuple:
        print("in get_lock_coordinates lock point: {}".format(self.lock_point))
        device_obj.screen.on()
        try:
            self.lock_point = config.lock_points[str(self.device)]
            print("DEBUG: LOCK POINT from CONFIG: ", self.lock_point)
        except Exception as e:
            print (e)
        stf2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/surface_view",30)
        if not self.lock_point:
            object_coordinates_arr, images_details = self.canvas.identify_lock_object(str(
                self.device_folder / "Screenshot.png"), self.screen_height, self.screen_width)
            print("object_coordinates_arr",object_coordinates_arr)
            for ind, image in enumerate(images_details):
                if ind == 0:
                    cv2.imwrite(str(self.device_folder / "lock.png"), image)
                else:
                    cv2.imwrite(str(self.device_folder / "lock_{}.png".format(ind)), image)

            if len(object_coordinates_arr) == 1:
                self.lock_point = object_coordinates_arr[0]
            else:
                self.lock_point = self.canvas.get_img_coordinate(self.device_folder / "lock.png",
                                                             large_image="")
        return self.lock_point

    def verify_in_start_home_screen_by_lock_png(self,img = None):
        lock = self.canvas.get_img_coordinate(self.device_folder / "lock.png",
                                                         large_image=img)
        if lock:
            return True
        return False


    # noinspection PyArgumentList
    def get_hubs_coordinates(self):
        # self.get_hub_coordinate()
        self.delete_point = self.canvas.get_img_coordinate(self.home_path / "delete_icon.png",
                                                           self.home_path /
                                                           "delete_chrome_page.png")
        # screenshot = partial(self.adb.screenshot_by_adb,
        #                      image_name=self.device_folder / "messaging_apps_page.png")
        # self.run_in_parallel(self.long_swipe_to_message_hub, screenshot)
        # screenshot = partial(self.adb.screenshot_by_adb,
        #                      image_name=self.device_folder / "plus_icon_page.png")
        # self.run_in_parallel(self.long_swipe_to_favorite, screenshot)
        # screenshot = partial(self.adb.screenshot_by_adb,
        #                      image_name=self.device_folder / "multimedia_apps_page.png")
        # self.run_in_parallel(self.long_swipe_to_camera_hub, screenshot)
        print("lock_point:", self.lock_point)
        print("favorite_point:", self.favorite_point)
        print("camera_point", self.multimedia_hub_point)
        print("delete_point", self.delete_point)
        print("message_point", self.message_hub_point)

    # noinspection PyArgumentList
    def get_icon_coordinates(self):
        self.camera_point = self.canvas.get_img_coordinate(
            self.device_folder / "Camera.png", self.device_folder / "multimedia_apps_page.png")
        self.gallery_point = self.canvas.get_img_coordinate(
            self.device_folder / "Gallery.png", self.device_folder / "multimedia_apps_page.png")
        self.plus_point = self.canvas.get_img_coordinate(self.device_folder / "Plus.png",
                                                         self.device_folder / "plus_icon_page.png")
        self.chrome_point = self.plus_point
        self.mail_point = self.canvas.get_img_coordinate(
            self.device_folder / "Email.png", self.device_folder / "messaging_apps_page.png")
        self.sms_point = self.canvas.get_img_coordinate(
            self.device_folder / "SMS.png", self.device_folder / "messaging_apps_page.png")
        print("plus_point", self.plus_point)
        print("chrome_point", self.chrome_point)
        print("sms_point", self.sms_point)
        print("mail point", self.mail_point)

    def crop_image_by_size(self, img_path, i):
        img = cv2.imread(str(img_path))
        crop_img = img[int(0.1 * self.screen_height):int(0.27 * self.screen_height),
                       0:self.screen_width]
        # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
        new_image = str(self.device_folder / "cropped_image{}.png".format(i))
        cv2.imwrite(new_image, crop_img)
        return new_image

    # noinspection PyArgumentList
    def press_on_hub_and_screenshot(self, i, hub_point, img_details):
        print("in press_on_hub_and_screenshot")
        points_array = [self.lock_point]
        points_array.extend(stf2.build_point_array(10,hub_point))
        points_array.append(self.lock_point)
        print("Debug:points_array {} ".format(points_array))
        swipe_to_hub = partial(device_obj.swipePoints, points_array, steps=70)
        # swipe_to_hub = partial(stf2.long_swipe_on_point_from_point, self.lock_point,
        #                        (hub_point[0], hub_point[1] + int(self.screen_height * 0.04)))
        screenshot = partial(self.save_hub_page_by_name, i, hub_point, img_details)
        self.run_in_parallel(swipe_to_hub, screenshot)


    def check_device_hub_page(self, i, img_name, name):
        print("inside check_device_hub_page function and the img name is:", img_name)
        if os.environ.get("DEVICE_STATUS") == "0":
            return
        if not Path(self.device_folder / img_name).exists():
            print(str(self.device_folder / img_name) + ": does not exist in device folder")
            os.rename(str(self.device_folder / "tmp{}.png".format(i)),
                      str(self.device_folder / img_name))
            # os.environ["DEVICE_STATUS"] += "it is new device,"
        else:
            print("DEBUG the file {} exist".format(self.device_folder / img_name))
            new_screenshot_without_full_screen = self.canvas.crop_images(self.device_folder /
                      "tmp{}.png".format(i), self.screen_width, self.screen_height, name)
            original_screenshot_without_full_screen = self.canvas.crop_images(
                self.device_folder / img_name, self.screen_width, self.screen_height, "original_" + name)
            status = self.canvas.compare_images(new_screenshot_without_full_screen,
                                                original_screenshot_without_full_screen)
            print("DEBUG status:", status)
            if status == "1":
                return
            else:
                os.environ["DEVICE_STATUS"] = "0"


    def save_hub_page_by_name(self, i, hub_point, img_details):
        sleep(4)
        check_output(
            "adb -s {} shell screencap -p /sdcard/Screenshot.png".format(self.device).split())
        sleep(1)
        check_output("adb -s {} pull /sdcard/Screenshot.png {}".format(
            self.device, str(self.device_folder / "tmp{}.png".format(
                i))).split())
        sleep(1)
        title_image = self.crop_image_by_size(self.device_folder / "tmp{}.png".format(i), i)
        name = self.get_text_from_image(title_image)
        print("DEBUG aut: extracted text from hub page - ",name)
        if "multimedia" in name:
            name = "multimedia_hub"
            self.check_device_hub_page(i, "multimedia_apps_page.png", name)
            self.multimedia_hub_point = (hub_point[0], hub_point[1])
        elif "messaging" in name:
            name = "messaging_hub"
            self.check_device_hub_page(i, "messaging_apps_page.png", name)
            self.message_hub_point = (hub_point[0], hub_point[1])
        elif "favorite" in name:
            name = "favorite_hub"
            self.check_device_hub_page(i, "plus_icon_page.png", name)
            self.favorite_point = (hub_point[0], hub_point[1])
        elif "Unlock" in name:
            name = "unlock_hub"
            self.unlock_point = (hub_point[0], hub_point[1])
        elif "Search" in name:
            name = "search_hub"
            self.search_hub = (hub_point[0], hub_point[1])
        elif "contact" in name:
            name = "contact_hub"
            self.contact_hub = (hub_point[0], hub_point[1])
        else:
            if not name:
                log_html.h_log("Hints are disabled on this device or hints are not of regular format")
                log_html.h_log("Enable Hints and add or remove some app icon in order to force "
                               "acquiring icon coordinates")
            else:
                print("DEBUG aut: Hub Name is not in config file: ",name)

        cv2.imwrite(str(self.device_folder / "{}.png".format(name)),
                    img_details)

    @staticmethod
    def get_max_x(object_coordinates_arr):
        print("object_coordinates_arr in get max x func", object_coordinates_arr)
        tmp = object_coordinates_arr[0][0]
        ind = None
        print("tmp start ", tmp)
        for i, v in enumerate(object_coordinates_arr):
            print(v[0])
            if tmp < v[0]:
                tmp = v[0]
                ind = i
        if ind is None:
            ind = 0

        return object_coordinates_arr[ind], ind

    # noinspection PyArgumentList
    def create_app_icon_by_crop(self, object_coordinates_arr, img_details, hub_point, hub_name):
        if hub_name == "plus_icon":
            self.number_of_favorite_shortcut = len(object_coordinates_arr)
            self.plus_point_last, self.index = self.get_max_x(object_coordinates_arr)
        for idx, app_point in enumerate(object_coordinates_arr):
            three_steps_swipe = partial(stf2.three_steps_swipe, self.lock_point, hub_point,
                                        app_point, hub_name, self.screen_height)
            screenshot = partial(self.crop_app_icon_and_save_by_name,
                                 img_details[idx], hub_name, idx)
            self.run_in_parallel(three_steps_swipe, screenshot)

    @staticmethod
    def get_text_from_image(img):
        print("in get_text_from_image")
        # Recognize text with tesseract for python
        name = pytesseract.image_to_string(Image.open(img))
        print("text extracted from image:", name)
        return name

    def crop_app_icon_and_save_by_name(self, icon_img_details, hub_name, idx):
        print("in crop_app_icon_and_save_by_name time {} + 5 seconds sleep".format(time()))
        sleep(4)
        print("starting crop_app_icon_and_save_by_name")
        start = time()
        check_output(
            "adb -s {} shell screencap -p /sdcard/Screenshot.png".format(self.device).split())
        sleep(1)
        check_output("adb -s {} pull /sdcard/Screenshot.png {}".format(
            self.device, str(self.device_folder / "tmp-{}.png".format(idx))).split())
        sleep(1)
        new_image = self.crop_image_by_size(self.device_folder / "tmp-{}.png".format(str(idx)),
                                            idx)
        name = self.get_text_from_image(new_image)
        if "com." in name:
            print("BUG ---> With Shortcut Name: ", name)
            log_html.h_log("BUG ---> With Shortcut Name: {}".format(name))
            log_html.h_log("    Failure screenshot --->: file:///{}".format(new_image))
        name_to_save = self.get_standard_file_name(name, hub_name)

        if hub_name == "plus_icon" and self.index == self.number_of_favorite_shortcut - 1:
            print("DEBUG plus icon:", name_to_save, self.index, self.number_of_favorite_shortcut
                  - 1)
            if name_to_save == "Plus":
                self.last_shortcut_status = "plus"
            else:
                self.last_shortcut_status = "shortcut"
        cv2.imwrite(str(self.device_folder / "{}.png".format(name_to_save)),
                    icon_img_details)
        end = time()
        print("crop_app_icon_and_save_by_name function execution time: = {}".format(end - start))

    @staticmethod
    def get_standard_file_name(name, hub_name):
        if "multimedia" in hub_name:
            for app_name in config.gallery_app:
                if name == app_name:
                    return config.gallery_app[0]
            for app_name in config.video_app:
                if name == app_name:
                    return config.video_app[0]
            for app_name in config.camera_app:
                if name == app_name:
                    return config.camera_app[0]
        elif "messaging" in hub_name:
            for app_name in config.message_app:
                if name == app_name:
                    return config.message_app[0]
            for app_name in config.email_app:
                if name == app_name:
                    return config.email_app[0]
        elif "plus_icon" in hub_name:
            for app_name in config.plus_icon:
                if name == app_name:
                    return config.plus_icon[0]

        for app_name in config.youtube_app:
            if name == app_name:
                return config.youtube_app[0]
        return name

    def swipe_on_all_app_icons(self):
        flag = 0
        for hub_path in config.hubs_screenshot_path:
            path = Path(hub_path)
            if path.exists():
                print("hub path:", hub_path)
                object_coordinates_arr, images_details = self.canvas. \
                    identify_all_app_objects_and_crop(hub_path, self.screen_width, self.screen_height)
                print(object_coordinates_arr)
                hub_point = None
                hub_name = None
                if "multimedia" in hub_path:
                    hub_point = self.multimedia_hub_point
                    hub_name = "multimedia"
                    flag = flag + 1
                elif "messaging" in hub_path:
                    hub_point = self.message_hub_point
                    hub_name = "messaging"
                    flag = flag + 1
                elif "plus_icon_page" in hub_path:
                    hub_point = self.favorite_point
                    hub_name = "plus_icon"
                    flag = flag + 1
                self.create_app_icon_by_crop(object_coordinates_arr, images_details,
                                             hub_point, hub_name)
            else:
                print("DEBUG: hub doesn't exists ", hub_path)
                raise Exception("in swipe_on_all_app_icons hub path doesn't exists")
        if flag != 3:
            print("DEBUG: in swipe_on_all_app_icons number hubs with apps less than 3 ")
            raise Exception("in swipe_on_all_app_icons - hub doesn't exists")

    def open_start_simple(self,behave=False,logging=True):
        print("in open_start_simple")
        try:
            if logging:
                log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
            check_output("adb -s {} shell am start com.celltick.lockscreen".format(self.device).split())
        except Exception as e:
            print("in exception")
            print(e)
        return self.verify_in_start_home_screen_by_elements(logging=False)

    @catch_any_error_and_return_false
    def verify_in_start_home_screen_by_elements(self,logging=False,behave=False):
        print("in verify_in_start_home_screen_by_elements")
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        device_obj.screen.on()
        lock_screen = stf2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/surface_view", 15)
        lock_screen2 = device_obj(resourceId="com.celltick.lockscreen:id/main_layout")
        device_obj.screen.on()
        menu_screen = stf2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/customization_prefs_label_id",15)
        print("lock_screen ",lock_screen,"lock_screen2 ",lock_screen2," ",menu_screen)
        if lock_screen or lock_screen2 and not menu_screen:
            print("returning True")
            return True
        l7 = len(device_obj(className="android.widget.FrameLayout"))
        l8 = len(device_obj(className="android.widget.FrameLayout").child())
        print("l7 ", l7, "l8 ", l8)
        if l7 == 1 and l8 == 0:
            print("returning True")
            return True
        print("returning False")
        return False
        # print("verify by pressing on lock and reading hints")
        # lock_hub_coordinates = self.lock_point if self.lock_point else self.guess_lock_point()
        # return not self.verify_no_hints_on_hub(lock_hub_coordinates, "Slide")

    def open_start(self,simple=None,logging=True):
        """
        open start application and verify it by search for the lock icon in start home page
        :return: True if start opened else False
        """
        print("in open_start")
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])

        try:
            print("open_start 111")
            check_output("adb -s {} shell am start com.celltick.lockscreen".format(self.device).split())
            sleep(3)
        except Exception as e:
            print(e)
        try:
            print("open_start 222")
            res = stf2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/surface_view", 180)
            if res:
                return True

            check_output("adb -s {} shell am start com.celltick.lockscreen".format(self.device).split())
            sleep(5)
            #device_obj(resourceId="com.celltick.lockscreen:id/surface_view").wait.exists(timeout=180000)
            if not res:
                device_obj.screen.off()
                device_obj.screen.on()
                sleep(3)
                res = stf2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/surface_view", 10)
                if not res:
                    return False
            if not simple:
                print("%%%%%%%%%%%%%not simple")
                if not self.adb.screenshot_by_adb(str(self.device_folder / "Screenshot.png")):
                    #self.write_internal_error_to_log("can not perform verifiaction that Start opened")
                    print("Debug: can not perform verification that start opened")
                    return False
                #lock_point = None
                if not self.lock_point:
                    print("no self lock point")
                    self.lock_point = self.get_lock_coordinates()
                if self.lock_point:
                    print("lock coordinates:", self.lock_point)
                    sleep(3)
                    return self.lock_point is not None
                self.check_tutorial_and_skip_button()
                try:
                    check_output("adb -s {} shell am start com.celltick.lockscreen".format(
                        self.device).split())
                    self.screen_on()
                    if not self.adb.screenshot_by_adb(str(self.device_folder / "Screenshot.png")):
                        #self.write_internal_error_to_log("can not perform verifiaction that Start opened")
                        print("Debug: can not perform verification that start opened")
                        return False
                except Exception as e:
                    print(e)
                if not self.lock_point:
                    self.lock_point = self.get_lock_coordinates()
                sleep(3)
                return self.lock_point is not None
            sleep(3)
            return True
        except Exception as e:
            print(e)
            return False


    def check_tutorial_and_skip_button(self):
        try:
            tutorial_skip = self.device_obj(resourceId="com.celltick.lockscreen:id/"
                                                       "tutorial_1_skip_label")
            tutorial_skip.click()
            keep_in_touch = self.device_obj(resourceId="com.celltick.lockscreen:id/skip_btn")
            keep_in_touch.click()

        except JsonRPCError as e:
            print("Skip buttons not display because start is all ready installed on device in "
                  "the same version as apk version", e)

    def screen_on(self,logging=False):
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        try:
            self.device_obj.screen.on()
            return True
        except JsonRPCError:
            return False

    def screen_off(self):
        try:
            self.device_obj.screen.off()
            return True
        except JsonRPCError:
            return False

    def unlock_device(self):
        """Turn on the screen and try to unlock using swipes"""
        self.device_obj.screen.on()
        sleep(1)
        try:
            self.device_obj().swipe.right()
        except JsonRPCError:  # Need to swipe up to unlock
            self.device_obj().swipe.up()

    def set_screen_state(self, screen_state: bool,logging = True):
        """
        turn screen to off or on according to screen_state param
        :param screen_state: 0 = off or 1 = on
        :return: the screen state after change by your desire
        """
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        print("screen state: {}".format(screen_state))
        if screen_state:
            print("inside screen on")
            self.device_obj.screen.on()
        else:
            self.device_obj.screen.off()
            print("inside screen off")
            #TODO : check function : check_if_the_screen_is_on_or_off
        return True#self.check_if_the_screen_is_on_or_off(screen_state)

    # def check_if_the_screen_is_on_or_off(self, screen_state: bool):
    #     """
    #     check_if_the_screen_is_on_or_off
    #     :param screen_state: screen_state: 0 = off or 1 = on
    #     :return: the screen state after change by your desire
    #     """
    #     sleep(2)
    #     # if screen_state:
    #     #     if self.device_obj.screen == "on":
    #     #         return True
    #     # else:
    #     #     if self.device_obj.screen == "off":
    #     #         return True
    #     if screen_state:
    #         screen_info = self.device_obj.info
    #         print("self.device_obj.screen status", screen_info["screenOn"])
    #         if screen_info["screenOn"] is True:
    #             self.unlock_device()
    #             return True
    #     else:
    #         screen_info = self.device_obj.info
    #         print("self.device_obj.screen status", screen_info["screenOn"])
    #         if not screen_info["screenOn"]:
    #             return True
    #     return False

    def uninstall_start_app(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.adb.uninstall_start_app()
        celltick_package = check_output("adb -s {} shell pm list packages "
                                        "com.celltick.lockscreen".format(self.device).split())
        if not celltick_package:
            self.device_obj.press.home()
            return True
        return False

    def get_hubs_dictionary(self):
        print("in get_hubs_dictionary")
        if not self.lock_point:
            self.lock_point = self.guess_lock_point()
        if not self.multimedia_hub_point:
            self.multimedia_hub_point = self.guess_multimedia_hub_point()
            print("guessed multimedia hub point: ", self.multimedia_hub_point)
        hubs_arr = {
            "lock_hub": (self.lock_point[0]+1, self.lock_point[1]+1) if
             self.check_point_valid(self.lock_point)  else None,
            "multimedia_hub": (self.multimedia_hub_point[0], self.multimedia_hub_point[1]) if
            self.check_point_valid(self.multimedia_hub_point) else None,
            "message_hub": (self.message_hub_point[0], self.message_hub_point[1]) if
            self.check_point_valid(self.message_hub_point) else None,
            "favorite_hub": (self.favorite_point[0], self.favorite_point[1]) if
            self.check_point_valid(self.favorite_point) else None,
            "unlock_hub": (self.unlock_point[0], self.unlock_point[1]) if
            self.check_point_valid(self.unlock_point) else None,
            "search_hub": (self.search_hub[0], self.search_hub[1]) if self.check_point_valid(self.search_hub)
            else None,
            "contact_hub": (self.contact_hub[0], self.contact_hub[1]) if
            self.check_point_valid(self.contact_hub) else None}
        return hubs_arr

    def check_point_valid(self,point):
        try:
            if point[0] != None and point[0] >= 0:
                return True
            return False
        except Exception as e:
            print(e)
            return False

    def verify_hubs_display(self):
        """
        check hubs existence
        :return: True if all hubs appear or False if some hub is missing
        """
        hubs_arr = self.get_hubs_dictionary()

        for k, v in hubs_arr.items():
            if v is None:
                print("This hub: {} is missing".format(k))
                return False, "This hub: {} is missing".format(k)
        return True

    def long_swipe_to_camera_hub(self):
        """
        make long swipe to camera hub for partial threads
        :return:
        """
        stf2.long_swipe_on_point_from_point(self.lock_point, (self.multimedia_hub_point[0] + 30,
                                                              self.multimedia_hub_point[1] + 30))

    def by_open_contact_hub_keypad_open(self):
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            self.device_obj.swipePoints([self.lock_point, self.contact_hub])
            sleep(3)
            activity = self.adb.get_recent_activity()
            for line in activity:
                if b"Recent #0" in line:
                    print(line)
                    if b"contacts" in line:
                        print("The swipe to contacts hub succeeds")
                        try:
                            self.device_obj(resourceId="com.android.contacts:id/dialButton")
                            print("the keypad was opened")
                            self.back()
                            return True
                        except JsonRPCError:
                            try:
                                self.device_obj(resourceId="com.android.contacts:id/dialpad_layout")
                                self.back()
                                return True
                            except JsonRPCError:
                                return False
                    print("The swipe to contacts hub failed")
        except Exception as e:

            print(e)
            return False

    def long_swipe_on_lock(self):
        """
        make long swipe on lock
        :return:
        """
        # self.device_obj.swipe(int(self.lock_point[0]), int(self.lock_point[1]),
        #                       int(self.lock_point[0]) + 1,
        #                       int(self.lock_point[1]) + 1, steps=600)
        self.device_obj.swipe(self.lock_point[0], self.lock_point[1],
                              self.lock_point[0] + 1,
                              self.lock_point[1] + 1, steps=600)

    def long_swipe_to_message_hub(self):
        """
        long_swipe_to_message_hub
        :return: None
        """
        stf2.long_swipe_on_point_from_point(self.lock_point, self.message_hub_point)

    def swipe_multimedia_apps_hub(self):
        """
        make swipe to multimedia apps hub
        :return:
        """
        print("--in swipe_multimedia_apps_hub")
        self.device_obj.swipe(self.lock_point[0], self.lock_point[1], self.multimedia_hub_point[0],
                              self.multimedia_hub_point[1])
        sleep(2)

    # def messages_of_permissions_displayed(self):
    #     print("in messages_of_permissions_displayed")
    #     log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    #     flag=0
    #     try:
    #         #log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    #         self.adb.uninstall_and_install_for_displayed_permission()
    #         self.open_start(simple=True)
    #         return self.allow_permissions()
    #     except Exception as e:
    #         self.write_internal_error_to_log()
    #         raise e

    # def allow_permissions(self):
    #     try:
    #         sleep(2)
    #         self.device_obj(resourceId="com.celltick.lockscreen:id/tutorial_1_skip_label").click()
    #     except:
    #         try:
    #             sleep(15)
    #             self.device_obj(
    #                 resourceId="com.celltick.lockscreen:id/tutorial_1_skip_label").click()
    #             flag = 1
    #         except JsonRPCError:
    #             adb_functions.restart_adb_server()
    #             try:
    #                 self.device_obj(
    #                     resourceId="com.celltick.lockscreen:id/tutorial_1_skip_label").click()
    #                 flag = 1
    #             except:
    #                 log_html.h_log("faled to click on Skip in Tutorilal screen")
    #                 print("Failed to reach to tutorial first skip page after install start")
    #                 return False
    #         if flag == 0:
    #             return False
    #     try:
    #         text = "Alowing all"
    #         res = self.allow_permissions_if_needed()
    #         # print("in second try")
    #         # text = "Allow Start to access photos, media, and files on your device?"
    #         # self.device_obj(text=text)
    #         # self.device_obj(text= "ALLOW").click()
    #         # text = "Allow Start to send and view SMS messages?"
    #         # self.device_obj(text=text)
    #         # self.device_obj(text="ALLOW").click()
    #         # text = "Allow Start to make and manage phone calls?"
    #         # self.device_obj(text=text)
    #         # self.device_obj(text="ALLOW").click()
    #         # text = "Allow Start to take pictures and record video?"
    #         # self.device_obj(text=text)
    #         # self.device_obj(text="ALLOW").click()
    #         # text = "Allow Start to access your contacts?"
    #         # self.device_obj(text=text)
    #         # self.device_obj(text="ALLOW").click()
    #         # text = "Allow Start to access this device's location?"
    #         # self.device_obj(text=text)
    #         # self.device_obj(text="ALLOW").click()
    #         # self.device_obj(resourceId="com.celltick.lockscreen:id/skip_btn").click()
    #         return res
    #
    #     except JsonRPCError:
    #         log_html.h_log("fialed tro click on {}".format(text))
    #         print("JsonRPCError happened, Failed to allow permission in {}".format(text))
    #         return False

    def allow_permissions_if_needed(self):
        print("in allow all permissions")
        arr = ["contacts", "video", "phone", "SMS", "location", "photos"]
        flag = True
        m = 5
        try:
            print("in try")
            res = stf2.verify_element_exists_by_resource_id("com.android.packageinstaller:id/permission_allow_button",30)
            print(1)
            if not res:
                print(2)
                self.back(2)
                res = stf2.verify_element_exists_by_resource_id(
                    "com.android.packageinstaller:id/permission_allow_button", 10)
                print(3)
                if not res:
                    print(4)
                    return res

            sleep(10)
            for i in range(6):
                print("allow permission num ",i)
                sleep(1)
                # '''
                # deprecated order verification
                # '''
                # if str(arr[i]).lower() not in str(device_obj(resourceId="com.android.packageinstaller:id/permission_message").text).lower():
                #     flag =  False
                if not self.allow_permission():
                    if not self.allow_permission():
                        return False
                m -= i
            if m > 0:
                print("number of permission screens less then 6")
                return False
            return flag
        except Exception as e:
            print(e)
            return False

    def allow_permission(self):
        print("in allow permission")
        try:
            self.device_obj(resourceId="com.android.packageinstaller:id/permission_allow_button").click()
            return True
        except Exception as e:
            print(e)
            return False

    def camera_default_app_opened(self):
        """
        check that camera in default opened by swipe to multimedia hub
        :return: True if opened or False if not
        """
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.swipe_multimedia_apps_hub()
        sleep(3)
        activity = self.adb.get_recent_activity()
        for line in activity:
            if b"Recent #0" in line:
                print("verifying camera opened: ",line)
                if b"Camera" in line or b"camera" in line or b"opencamera" in line:
                    self.back(2)
                    return True
                return False
        try:
            self.device_obj(resourceId="com.sec.android.app.camera:id/base_layout")
            self.back()
            return True
        except JsonRPCError as e:
            print(e)
            return False


        # If failed to verify by element , try to verify by recent activity

    def swipe_to_messaging_apps_hub(self):
        """
        make swipe to messaging apps hub
        :return: None
        """
        self.device_obj.swipe(int(self.lock_point[0]), int(self.lock_point[1]),
                              self.message_hub_point[0],
                              self.message_hub_point[1])
        sleep(2)

    def messages_default_app_opened(self):
        """
        check that message app in default opened by swipe to messages hub
        :return: the text inside messages app
        """
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.swipe_to_messaging_apps_hub()
        sleep(1.5)
        activity = self.adb.get_recent_activity()
        for line in activity:
            if b"Recent #0" in line:
                print(line)
                if b"mms" in line or b"Message" in line or b"cmail" in line:
                    return True
                return False

    def slide_to_favorite_reached_to_device_home_screen(self):
        """
        check while slide to favorite hub start app closed and you now in device home screen
        :return: True if in device home screen, False if not
        """
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.device_obj.swipe(self.lock_point[0], self.lock_point[1], self.favorite_point[0],
                              self.favorite_point[1])
        sleep(2)
        return self.verify_device_home_screen_display(unlock_status=0)

    def verify_device_home_screen_display(self,unlock_status=0, ratio=None):
        """
        verify if device home screen display
        :return:
        """
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        # try:
        #     device_obj(className="android.view.View").wait.exists(timeout=30000)
        #     if len(self.device_obj(packageName="com.android.launcher3")) > 0 or len(device_obj(className="android.view.View")) > 0:
        #         return True
        #     return False
        # except Exception as e:
        #     print(e)
        #     return False
        sleep(2)
        self.device_obj.press.home()
        sleep(1)
        self.back(3)
        sleep(2)
        if not ratio:
            ratio = 0.7
        try:
            if unlock_status == 1 and self.device == "Z9SGJJLVPZOBH6DU":
                self.unlock_device()
                self.device_obj.swipe(int(self.screen_width / 2), int(self.screen_height * 0.75),
                                      self.screen_width - 10, int(self.screen_height * 0.75))
            self.device_obj.dump(str(self.device_folder / "screen.xml"))
            sleep(1)
            tree = ET.parse(self.home_screen_xml)
            root = tree.getroot()
            xml_str1 = ET.tostring(root, encoding='utf8', method='xml')
            tree = ET.parse(str(self.device_folder / "screen.xml"))
            root = tree.getroot()
            xml_str2 = ET.tostring(root, encoding='utf8', method='xml')
            similarity_perc = SM(None, xml_str1, xml_str2).ratio()
            print("percent of similarity of device home screen ",similarity_perc )
            if similarity_perc > ratio:
                return True
            else:
                return False
        except Exception as e:
            print(e)
            return None

    @catch_any_error_and_return_false
    def verify_in_device_home_screen_by_elements(self,logging=False):
        print("in verify_in_device_home_screen_by_elements")
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        try:
            sleep(5)
            l1 = len(device_obj(resourceId="com.celltick.lockscreen:id/main_layout"))
            l2 = len(device_obj(resourceId="com.celltick.lockscreen:id/customization_prefs_label_id"))
            print("l1 = ", l1)
            print("l2 = ", l2)
            l3 = len(device_obj(className="android.view.ViewGroup"))
            l4 = len(device_obj(resourceId="com.android.launcher3:id/apps_customize_pane_content"))
            l5 = len(device_obj(resourceId="com.android.quicksearchbox:id/search_widget_text"))
            print("l3 = ", l3)
            print("l4 = ", l4)
            print("l5 = ", l5)
            if l1 == 0 and l2 == 0 and (l3 > 0 or l4 > 0 or l5):
                return True
            else:
                l6 = len(device_obj(resourceId="com.google.android.apps.nexuslauncher:id/search_container_hotseat"))
                if l6 > 0:
                    return True
                return self.verify_device_home_screen_display()
        except Exception as e:
            print(e)
            return None

    @catch_any_error_and_return_false
    def slide_to_unlock_hub_reached_to_device_home_screen(self):
        """
        check while slide to unlock hub start app closed and you now in device home screen
        :return: True if in device home screen, False if not
        """
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        if not self.lock_point:
            self.lock_point = self.guess_lock_point()
        if not self.unlock_point:
            self.unlock_point = self.guess_unlock_point()
        if not self.lock_point or not self.unlock_point:
            print("in slide_to_unlock_hub_reached_to_device_home_screen lock or unlock point "
                  "missing")
            return False
        try:
            sleep(2)
            print("DEBUG: lock point = {} , unlock point = {} ".format(self.lock_point,self.unlock_point))
            self.device_obj.swipe(self.lock_point[0], self.lock_point[1], self.unlock_point[0],
                                  self.unlock_point[1],steps=150)
            sleep(5)
            return self.verify_device_home_screen_display(unlock_status=0)
        except Exception as e:
            print("can not perform home screen verification")
            #self.write_internal_error_to_log("can not perform home screen verification")
            print(e)
            return False

    def swipe_menu(self, logging=True):
        """
        swipe on menu and verify it by cv2 if "Settings" displayed
        :return: Settings point , or None if settings not displayed
        """
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])

        print("menu point", self.menu_point)
        self.swipe_menu_stf()
        sleep(2)
        res = self.get_settings_button_coordinates()
        if not res:
            self.back(2)
            self.device_obj.wakeup()
            sleep(2)
            self.back(2)

            sleep(1)
            self.swipe_menu_stf()
            res = self.get_settings_button_coordinates()
            if not res:
                self.device_obj.screen.on()
                self.device_obj.screen.off()
                sleep(2)
                self.swipe_menu_stf()
                res = self.get_settings_button_coordinates()
            return  res
        else:
            return res

    def back(self, times: int=1):
        print("in back")
        print("pressing back {times}")
        for i in range(times):
            self.device_obj.press.back()

    def swipe_menu_stf(self):
        self.device_obj.swipe(self.menu_point[0], self.menu_point[1],
                              int(self.menu_point[0] / 4), self.menu_point[1])

    def get_settings_button_coordinates(self,text="Settings"):
        print("in get_settings_button_coordinates ")
        print("to find : ", text)
        try:
            settings_obj = self.device_obj(text=text).info
            settings_bounds = settings_obj["bounds"]
            print(text," Bounds(catching setting point in swipe_menu): ", settings_bounds)
            point = (int((settings_bounds["left"] + settings_bounds["right"]) / 2),
                                       int((settings_bounds["top"] + (settings_bounds["bottom"])) / 2))
            if text == "Settings":
                self.settings_point = point
                print("Settings point:", self.settings_point)
                return self.settings_point
            else:
                print(text, " point: ",point)
                return point
        except Exception as e:
            print(e)
            return None


    def swipe_screen(self):
        try:
            self.device_obj.swipe(int(self.screen_width * 0.93),
                    int(self.screen_height*0.6),int(self.screen_width*0.35),
                                  int(self.screen_height*0.6))
            return True
        except Exception as e:
            print (e)

    def click_on_button_in_menu_screen(self,button_name):
        try:
            settings_obj = self.device_obj(text=button_name).click()
            return True
        except Exception as e:
            print(e)
            return False

    def verify_in_settings_screen(self):
        try:
            if str(device_obj(resourceId="android:id/title").text).lower() == "settings":
                return True
            elif str(device_obj(className=config.general_class_names.framw).child(
                 className=config.general_class_names.textv).text).lower() == "settings":
                return True
            return False
        except Exception as e:
            print(e)
            return False

    def click_on_setting_button(self,menu = "Settings",logging=True,behave=False):
        """
        click on setting button and verify Setting page opened
        :return: the text from Setting page
        """
        print("in click_on_setting_button")
        try:
            if logging:
                print("in logging mm")
                log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
            self.device_obj(text=menu).click()
            sleep(2)
            is_in_settings =  self.verify_in_settings_screen()
            print("is in settings? ",is_in_settings)
            return is_in_settings
            # if not self.settings_point or menu != "Settings":
            #     self.settings_point = self.get_settings_button_coordinates(menu)
            # sleep(2)
            # self.device_obj.click(self.settings_point[0], self.settings_point[1])
            # sleep(3)
            # menu_text = device_obj(className=config.general_class_names.framw).child(
            #     className=config.general_class_names.textv).text
            # print("the text : {}".format(menu_text))
            # print("menu: ", menu)
            # res = menu_text == menu
            # print("result  menu_text == menu ",res)
        except Exception as e:
            print("in exceprion")
            print(e)
            return None

    def click_on_notification_selection(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        try:
            self.scroll_and_click_by_text("Notification Selection")
            if self.device_obj(resourceId="android:id/alertTitle").text in " Notification Selection ":
                return True
            return False
        except Exception as e:
            self.write_internal_error_to_log("can not perform notificaton selection screen verification")
            raise e

    def select_item_in_settings_by_text(self,title):
        try:
            self.scroll_and_click_by_text(title)
        except Exception as e:
            self.write_internal_error_to_log("can not select item in settongs")
            print (e)
            return  False
        return True

    def select_item_in_Setup_Your_Widget(self,title,logging=False):
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        print("in select_item_in_Setup_Your_Widget")
        try:
            self.scroll_and_click_by_text(title)
        except Exception as e:
            self.write_internal_error_to_log("can not select widget")
            print(e)
            return False
        print("returning True")
        return True


    def scroll_and_click_setup_starters(self):
        """

        :return:
        """
        try:
            self.scroll_and_click_by_text("Setup Starters")
        except JsonRPCError:
            print("The Setup Starters Button not Display")
        sleep(1)

    def verify_starter_displayed(self) -> tuple:
        starter_point = ()
        for _ in range(2):
            starter_point = self.canvas.verify_action(
                self.device_folder / "huffington.png", large_image="")
            print("huffington starter point:", starter_point)
            if starter_point:
                return starter_point
            self.device_obj.swipe(50, 1300, 50, 500)
        return starter_point

    @catch_any_error_and_return_false
    def scroll_and_click_by_text(self, text , scrollable = True,logging=False):
        print("in scroll_and_click_by_text: ",text)
        """
        scroll to some text and click on it
        :param text: the text you want to scroll too
        :return: None
        """
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        try:
            print("the text to scroll and click is: ", text)
            try:
                self.device_obj(scrollable=scrollable).scroll.to(text=str(text))
            except Exception as e:
                print(e)
            self.device_obj(text="{}".format(text)).click()
            sleep(1)
        except Exception as e:
            print(e)
            return False
        return True

    def turn_on_starter(self, starter_name):
        self.device_obj(scrollable=True).scroll.to(text="{}".format(starter_name))
        for view in self.device_obj(className="android.widget.RelativeLayout"):
            if view.child(resourceId="com.celltick.lockscreen:id/title").text in starter_name:
                starter_status = view.child(
                    resourceId="com.celltick.lockscreen:id/toggleButton_Enabled").info
                if not starter_status["checked"]:
                    view.child(
                        resourceId="com.celltick.lockscreen:id/toggleButton_Enabled").click()
                checked_box = view.child(
                    resourceId="com.celltick.lockscreen:id/toggleButton_Enabled").info
                print(checked_box)
                self.back(2)
                return checked_box["checked"]

    def swipe_to_plus_icon_in_favorite(self, remove_place):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        plus_point = self.plus_point_place()
        if self.last_shortcut_status == "plus":
            self.fast_swipe_to_last_shortcut(plus_point)
        else:
            points = [tuple(p) for p in (self.lock_point, self.favorite_point,
                                         plus_point, (plus_point[0] + 2, plus_point[1] + 2),
                                         (plus_point[0] + 3, plus_point[1] + 3))]
            self.device_obj.swipePoints(points)
            if not self.remove_the_last_delete(plus_point, remove_place):
                return None
            self.back()
            self.fast_swipe_to_last_shortcut(plus_point)
        return self.verify_it_choose_app_shortcut_page()

    def fast_swipe_to_last_shortcut(self, plus_point):
        try:
            points = [tuple(p) for p in (self.lock_point, self.favorite_point,
                                         plus_point)]
            self.device_obj.swipePoints(points)
            return True
        except Exception as e:
            print(e)
            return None

    def add_shortcut(self, func_name):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        print("DEBUG add shortcut {}".format(func_name))
        sleep(1)
        status = self.canvas.enter_app_name_for_shortcut(str("chrome"), self.screen_width,
                                                         self.screen_height)
        # print("aaaaaaaaa 11")
        # print("aaaaaaaaaaa status: ",status)
        if not status:
            return False
        sleep(1)
        # print("aaaa 222")
        try:
            # print("aaaa 33")
            self.device_obj(className="android.widget.RelativeLayout")[1].click()
            # print("aaaaa 3.5")
            sleep(2)
            if func_name == "via plus":
                # print("aaaaaaaaaaaa 3.6")
                self.adb.screenshot_by_adb(Path(self.device_folder) / "delete_page.png")
                # print("aaaaaaaaaaaaaaaa 3.7")
            sleep(0.5)
            return True
        except (IndexError, JsonRPCError):
            # print("aaaaaaa 444")
            print("Click On Chrome Icon Failed")
            return False

    def open_gallery(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.complex_swipe((self.lock_point, self.multimedia_hub_point,
                            self.gallery_point))
        sleep(1.5)
        activity = self.adb.get_recent_activity()
        for line in activity:
            if b"Recent #0" in line:
                print(line)
                if b"gallery" in line:
                    return True
                return False

    def open_camera_by_icon(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.complex_swipe((self.lock_point, self.multimedia_hub_point, self.camera_point))
        sleep(3)
        activity = self.adb.get_recent_activity()
        for line in activity:
            if b"Recent #0" in line:
                print(line)
                if b"Camera" in line or b"camera" in line or b"opencamera" in line:
                    return True
        if self.device_obj(resourceId="com.sec.android.app.camera:id/base_layout").exists:
            self.back()
            return True
        return False

    def open_messages_by_shortcut(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.complex_swipe((self.lock_point, self.message_hub_point, self.sms_point))
        sleep(2)
        activity = self.adb.get_recent_activity()
        for line in activity:
            if b"Recent #0" in line:
                print(line)
                if b"mms" in line or b"Message" in line or b"cmail" in line:
                    return True
                return False

    def verify_start_main_screen_reappear(self,logging=True):
        print("in verify_start_main_screen_reappear")
        try:
            if logging:
                log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            for _ in range(3):
                sleep(0.5)
                self.back()
            lock_point = self.canvas.verify_action(self.device_folder / "lock.png", large_image="")
            return lock_point
        except Exception as e:
            self.write_internal_error_to_log("can not verify device screen")
            print(e)
            return False

    # noinspection PyArgumentList
    def verify_shortcut_displayed(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        sleep(2)
        try:
            plus_point = self.plus_point_place()
            chrome_shortcut_arr = [self.lock_point, self.favorite_point, plus_point]
            print("chrome_shortcut_arr:", chrome_shortcut_arr)
            self.device_obj.swipePoints(chrome_shortcut_arr, steps=50)
            sleep(2)
            activity = self.adb.get_recent_activity()
            print("in verify_shortcut_displayed , activity ", activity)
            for line in activity:
                if b"Recent #0" in line:
                    print(line)
                    if b"chrome" in line:
                        self.back()
                        return True
            sleep(2)
            self.back()
            long_swipe_chrome_shortcut = partial(stf2.three_steps_swipe, self.lock_point,
                                                 self.favorite_point, plus_point,
                                                 "plus_icon", self.screen_height)
            screenshot = partial(self.adb.screenshot_by_adb_for_chrome,
                                 image_name=self.device_folder / "Chrome_page.png")
            self.run_in_parallel(long_swipe_chrome_shortcut, screenshot)
            new_image = self.crop_image_by_size(self.device_folder / "Chrome_page.png", 1)
            name = self.get_text_from_image(new_image)
            print("name of shortcut:", name)
            if "Chrome" in name:
                return True
            return False
        except Exception as e:
            print(e)
            return False

    def verify_edit_mode_after_swipe_to_shortcut_in_messages(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        # self.complex_swipe((self.lock_point, self.message_hub_point,
        #                     self.mail_point,
        #                     (self.mail_point[0] + 2, self.mail_point[1] + 2),
        #                     (self.mail_point[0] + 3, self.mail_point[1] + 3)))
        #stf2.long_swipe_on_point_from_point(pt_from=
        sleep(1.5)
        large_image = self.adb.take_snapshot_with_sleep()
        if not large_image:
            return None
        return self.canvas.search_text_in_image(large_image, "Edit mode")

    def complex_swipe(self, arr_points , steps = 100):

        # points = [tuple(p) for p in args]
        self.device_obj.swipePoints(arr_points,steps = steps)

    def press_on_plus_icon_in_mail_place(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.device_obj.click(self.mail_point[0], self.mail_point[1])
        return self.verify_it_choose_app_shortcut_page()

    def remove_shortcut(self, remove_place):
        """
        remove mail shortcut
        :return: chrome point
        """
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            plus_point = self.plus_point_place()
            self.back()
            if remove_place == "mail":
                arr = [self.lock_point, self.message_hub_point].extend(stf2.get_points_arr_close_to_point(10,self.mail_point))
                self.complex_swipe(arr)
            elif remove_place == "favorite" or remove_place == "setting":
                arr = [self.lock_point, self.favorite_point].extend(stf2.get_points_arr_close_to_point(10,plus_point))
                self.complex_swipe(arr)
                # self.complex_swipe([self.lock_point, self.favorite_point, plus_point,
                #                     (plus_point[0] + 2, plus_point[1] + 2),
                #                     (plus_point[0] + 3, plus_point[1] + 3)], steps=200)
            sleep(1)
            self.remove_the_last_delete(plus_point, remove_place)
            sleep(2)
            self.adb.screenshot_by_adb(self.device_folder / "after_remove.png")
            self.back()
            result = self.canvas.compare_images(self.device_folder / "after_remove.png",
                                                self.device_folder / "before_remove.png")
            print("FINISHED remove_shortcut")
            if result == "0":
                result = None
            return result
        except Exception as e:
            print("!!!!!!!!!!!!!!!!!EEEEEEEEEEEEEEEException!!")
            print("EXCEPTION IS:::: ")
            print(e)
            self.write_internal_error_to_log()
            raise e

    def remove_the_last_delete(self, plus_point, remove_place):
        try:
            self.adb.screenshot_by_adb(self.device_folder / "delete_{}_page.png".format(remove_place))
            sleep(1)
            object_coordinates_arr, images_details = self.canvas.identify_delete_object(
                str(self.device_folder / "delete_{}_page.png".format(remove_place)), self.screen_height)
            print("shortcut in {}".format(remove_place), len(object_coordinates_arr), object_coordinates_arr)
            item = None
            if object_coordinates_arr:
                if len(object_coordinates_arr) > 1:
                    x, item = self.find_nearest_delete_to_shortcut(object_coordinates_arr,
                                                                   plus_point[0])
                    # for image in images_details:
                    print("x, item :", x, item, type(item))
                    print("DEBUG delete", object_coordinates_arr[item])
                    cv2.imwrite(str(self.device_folder / "delete_icon.png"), images_details[item])
                else:
                    cv2.imwrite(str(self.device_folder / "delete_icon.png"), images_details[0])
                self.adb.screenshot_by_adb(self.device_folder / "before_remove.png")
                w, h = self.canvas.calculate_center_image(str(self.device_folder / "delete_icon.png"))
                # x, y = self.canvas.get_img_coordinate(self.device_folder / "delete_icon.png",
                #                                       self.device_folder /
                #                                       "after_remove.png")
                if item:
                    self.device_obj.click(object_coordinates_arr[item][0] + int(w),
                                          object_coordinates_arr[item][1] + int(h))
                else:
                    self.device_obj.click(object_coordinates_arr[0][0] + int(w),
                                          object_coordinates_arr[0][1] + int(h))
            return True
                    # self.device_obj.click(x + int(w), y + int(h))
        except Exception as e:
            print(e)
            return None

    def plus_point_place(self):
        if type(self.plus_point) is tuple and self.plus_point:
            if self.plus_point[0]:
                print("a")
                if self.plus_point_last[0]:
                    print("b")
                    if self.plus_point[0] >= self.plus_point_last[0]:
                        print("c")
                        plus_point = self.plus_point
                        return plus_point
                    else:
                        print("d")
                        plus_point = self.plus_point_last
                        return plus_point
                else:
                    print("e")
                    plus_point = self.plus_point
            else:
                print("f")
                plus_point = self.plus_point_last
        else:
            print("f")
            plus_point = self.plus_point_last
            print("plus point place:", plus_point)
        return plus_point

    @staticmethod
    def find_nearest_delete_to_shortcut(delete_arr, value):
        x_delete_arr = []
        for item in delete_arr:
            x_delete_arr.append(item[0])
        tmp = abs(x_delete_arr[0] - value)
        x = None
        ind = None
        for item in range(len(x_delete_arr)):
            tmp2 = abs(x_delete_arr[item] - value)
            if tmp2 <= tmp:
                tmp = tmp2
                x = x_delete_arr[item]
                ind = item
        return x, ind

    def click_on_favorite_apps_and_contacts(self):
        """
        click on set your favorite apps & contacts button and verify it by text of "Select your
        shortcut"
        :return: "Select your shortcut" if click succeed
        """
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.scroll_and_click_by_text("Set Your Favorite Apps & Contacts")
        sleep(1)
        return self.device_obj(resourceId="android:id/title").text

    def click_on_my_apps(self):
        """
        click on "My Apps" button and verify it by text from the next page
        :return: the text from next page after click
        """
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.device_obj(text="My Apps").click()
        return self.device_obj(
            resourceId="com.celltick.lockscreen:id/shortcuts_customize_name").text

    def click_on_plus_in_select_shortcut_menu(self):
        # click on last position
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        level_1 = "android.widget.RelativeLayout"
        level_2 = "android.widget.LinearLayout"
        level_3 = "com.celltick.lockscreen:id/shortcut_title"
        plus_cells = self.device_obj(className="android.widget.RelativeLayout")
        shortcut_in_settings = device_obj(className=level_1).child(className=level_2).\
            child(resourceId=level_3)
        for item in shortcut_in_settings:
            if "Chrome" in item.text:
                # #######################################################
                # Here i'am replacing "Chrome" with any other application
                #########################################################
                item.click()
                self.device_obj(className="android.widget.RelativeLayout")[1].click()
        self.device_obj(className="android.widget.RelativeLayout")[len(plus_cells) - 1].click()

        return self.verify_it_choose_app_shortcut_page()

    def verify_it_choose_app_shortcut_page(self):
        try:
            if self.device_obj(
                    resourceId="com.celltick.lockscreen:id/shortcuts_customize_title").text:
                return True
            else:
                return False
        except JsonRPCError:
            try:
                self.device_obj(text="Choose app shortcut")
                return True
            except JsonRPCError:
                print("The page of choose app shortcut not displayed")
                return False

    def verify_at_least_2_plus_icon_display(self):
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            number_of_plus_icon = self.canvas.identify_small_image_occurrences_in_large_image(
                self.device_folder / "Plus.png",
                self.device_folder / "plus_icon_page.png")
            print("number_of_plus_icon", number_of_plus_icon)
            return number_of_plus_icon
        except Exception as e:
            self.write_internal_error_to_log("can not perform verication")
            raise e

    def contact_us_contains_3_options(self):
        """
        This method check the contact us page that 3 options display and click on The First One
        :return:
        """
        print("in contact_us_contains_3_options")
        try:
            title_id = "android:id/title"
            res = 0
            arr1 = ["bug", "better", "report"]
            arr2 = []
            for item in device_obj(resourceId=title_id):
                arr2.append(str(item.text).lower())
            print("arr2 ", arr2)
            for item in arr1:
                for item2 in arr2:
                    if item.lower() in item2:
                        res += 1
            return res
        except Exception as e:
            print(e)
            return 0

    def click_on_disable_start(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.scroll_and_click_by_text("Disable Start")
        return self.device_obj(resourceId="com.celltick.lockscreen:id/title").text

    def disable_possible_periods_appear(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        sleep(1)

        self.device_obj(resourceId="com.celltick.lockscreen:id/disable_start_btn").click()
        res = self.verify_disable_periods_appear()
        try:
            self.device_obj(resourceId="com.celltick.lockscreen:id/ok_btn").click()
            return res
        except Exception as e:
            print(e)
            return False

    def verify_disable_periods_appear(self):
        try:
            # self.device_obj(resourceId="com.celltick.lockscreen:id/ok_btn").wait(3000)
            stf2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/ok_btn", 7)
            if len(self.device_obj(resourceId="com.celltick.lockscreen:id/ok_btn")) > 0:

                return True
            return False
        except Exception as e:
            return False

    def by_choose_time_period_start_closed_and_disable(self):
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            radio_bt_status = self.device_obj(
                resourceId="com.celltick.lockscreen:id/disable_start_for_1_week_id")
            print(radio_bt_status.checked)
            self.device_obj(
                resourceId="com.celltick.lockscreen:id/disable_start_for_1_week_id").click()
            print(radio_bt_status.checked)
            self.device_obj(resourceId="com.celltick.lockscreen:id/ok_btn").click()
            return self.verify_device_home_screen_display(unlock_status=0)
        except Exception as e:
            self.write_internal_error_to_log()
            raise e

    # def contact_us(self,logging=True,veification_only=False):
    #     """
    #     :return:
    #     """
    #     if logging:
    #         log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
    #     if not veification_only:
    #         self.scroll_and_click_by_text("Contact Us")
    #     try:
    #         return self.verify_in_contact_us_screen()
    #     except Exception as e:
    #         self.write_internal_error_to_log()
    #         raise e

    def verify_in_contact_us_screen_3_options(self):
        if self.contact_us_contains_3_options() != 3:
            print("There Is No 3 Options In Contact Us Page")
            return False
        print("the len is 3")
        return True#self.verify_contact_us_has_email_us_popup()

    def check_tutorial_displayed(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        try:
            self.scroll_and_click_by_text("Tutorial")
            #self.device_obj(resourceId="com.celltick.lockscreen:id/tutorial_1_title").wait(10000)
            res = self.verify_tutorial_opened()
            return res
        except Exception as e:
            self.write_internal_error_to_log()
            raise e

    def verify_tutorial_opened(self):
        res_0 = stf2.verify_element_exists_by_resource_id("com.celltick.lockscreen:id/tutorial_1_title", 10)
        res_1 = self.skip_tutorial_by_ok()
        return res_0 or res_1

    def skip_tutorial_by_ok(self):
        print("in skip_tutorial_by_ok")
        for i in range(1, 6):
            try:
                self.device_obj(resourceId="com.celltick.lockscreen:id/tutorial_{}_title".
                                format(i))
                sleep(1)
                if i == 1:
                    self.device_obj(
                        resourceId="com.celltick.lockscreen:id/tutorial_1_btn_ok").click()
                elif i == 5:
                    self.device_obj(
                        resourceId="com.celltick.lockscreen:id/tutorial_5_start").click()
                else:
                    self.device_obj(text="Next").click()
                sleep(1)
            except JsonRPCError:
                print("JsonRPCError in skip_tutorial_by_ok")
                print("The {} Page Of Tutorial Not Displayed".format(i))
                return False
        return True

    def verify_contact_us_has_email_us_popup(self):
        print("in verify_contact_us_has_email_us_popup")
        sleep(1)
        contact_us_popup = self.device_obj(
            resourceId="com.celltick.lockscreen:id/dfd_description_text").text
        if contact_us_popup == "Email us and we will squash it!":
            self.verify_start_main_screen_reappear()
            return True
        return False

    def click_on_advanced_settings(self,behave=False,logging=True):
        print("in click_on_advanced_settings")
        try:
            if logging:
                log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
            self.scroll_and_click_by_text("Advanced Settings")
            if self.device_obj(text="Full Screen").text is not None:
                print("verified - TRUE")
                return True
            return False
        except Exception as e:
            print(e)
            return False

    def verify_menu_is_absent_on_main_screen(self,behave=False):
        print("in verify_menu_is_absent_on_main_screen")
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
            to_return = True
            current_img = str(self.device_folder / "after_remove_menu.png")
            previous_img = str(self.device_folder / "Screenshot.png")
            print("current_img path ",current_img)
            print("previous image path ",previous_img)
            #self.verify_start_main_screen_reappear()
            cur2 = self.adb.screenshot_by_adb(current_img)
            print("image was saved by adb_screenshot as ",cur2)
            result = canvas.compare_images(current_img, previous_img)
            if result == 1 or None:
                to_return = False
            # self.swipe_menu()
            # self.click_on_setting_button()
            # self.scroll_and_click_by_text("Advanced Settings")
            # self.enable_disable_advanced_settings("Show Settings Icon", True)
            # self.verify_start_main_screen_reappear()
            return to_return
        except Exception as e:
            print(e)
            return False
            #self.write_internal_error_to_log()
            #raise e

    # noinspection PyArgumentList
    def verify_no_hints_on_hub(self, hub_coordinates, hint):
        print("hub coordinates ",hub_coordinates)
        print("in verify_no_hints_on_hub")
        try:
            if not self.lock_point:
                self.lock_point = self.guess_lock_point()
        except Exception as e:
            print("error in frist try")
            print(e)
        exists = False
        pt_from = self.lock_point
        try:
            if hub_coordinates:
                pt_on = (int(hub_coordinates[0]), int(hub_coordinates[1]))
                img = self.device_folder / "select_options.png"
                long_press = partial(stf2.long_swipe_on_point_from_point, pt_from, pt_on,back_to_from=True)
                screenshot = partial(self.adb.screenshot_by_adb, image_name=img)
                self.screen_on(logging=False)
                self.run_in_parallel(long_press, screenshot)
                sleep(1)
                exists = hint in self.get_text_from_image(img)
                #exists = self.canvas.search_text_in_image(img, hint)
            return not exists
        except Exception as e:
            print("error in verify_no_hints_on_hub")
            print(e)
            return False

    def scroll_on_advanced_settings_list_and_check(self, text, to_enable):
        print("scroll_on_advanced_settings_list_and_check")
        result = False
        if to_enable == True or int(to_enable) == 1 :
            to_enable = 1
        else:
            to_enable = 0
        relative_layout_class = config.adv_set_relative_layout_class
        linear_layout_class = config.adv_set_linear_layout_class
        chk_box_class = config.adv_set_chk_box_class
        for idx, box in enumerate(self.device_obj(className=relative_layout_class)):
            box_child = box.child(className="android.widget.CheckedTextView")
            box_child_txt = box_child.text
            assert isinstance(box_child_txt, str)
            print(box_child_txt)
            if box_child_txt == text:
                chk = box.sibling(className=linear_layout_class).child(className=chk_box_class)
                is_checked = chk.checked
                if is_checked:
                    if not to_enable:
                        chk.click()
                else:
                    if to_enable:
                        chk.click()
                chk = box.sibling(className=linear_layout_class).child(className=chk_box_class)
                is_checked = chk.checked
                os.environ["RAV_ADVANCED_SETTINGS"] = text + "#" + str(is_checked)
                result = True
        return result

    def bring_device_to_original_state(self):
        advanced = os.environ.get("RAV_ADVANCED_SETTINGS")
        try:
            if advanced:
                if len(advanced.split("#")) == 2:
                    text, to_enable = advanced.split("#")
                    if "False" in to_enable:
                        to_enable = 0
                    elif "True" in to_enable:
                        to_enable = 1
                    if "Show Settings Icon" in text and int(to_enable) == 0:
                        self.get_to_advanced_settings()
                        self.enable_disable_advanced_settings(text,1, logging=False)
                    elif "Show Hints" in text and int(to_enable) == 0:
                        self.get_to_advanced_settings()
                        self.enable_disable_advanced_settings(text,1, logging=False)
                    elif "Full Screen" in text and int(to_enable) == 1:
                        self.get_to_advanced_settings()
                        self.enable_disable_advanced_settings(text,0, logging=False)
        except Exception as e:
            print(e)

    def get_to_advanced_settings(self):
        self.back(2)
        self.swipe_menu(logging=False)
        self.click_on_setting_button(logging=False)
        self.click_on_advanced_settings(logging=False)

    def enable_disable_advanced_settings(self, text, to_enable,logging=True, behave=False):
        print("in enable_disable_advanced_settings")
        try:
            if logging:
                log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            try:
                self.device_obj(scrollable=True).scroll.vert.toBeginning(steps=100, max_swipes=1000)
            except Exception as e:
                print(e)
            print(1)
            result = self.scroll_on_advanced_settings_list_and_check(text, to_enable)
            print(2)
            if not result:
                print(3)
                self.device_obj.swipe(int(self.screen_width/2), int(self.screen_height*0.8),
                                      int(self.screen_width/2), self.screen_height*0.3, steps=50)
                print("4")
                result = self.scroll_on_advanced_settings_list_and_check(text, to_enable)
                print(5)
            return result
        except Exception as e:
            print(6)
            print(e)
            return False

    def verify_hints_absent(self):
        print("in verify_hints_absent")
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            flag = True
            hubs_arr = self.get_hubs_dictionary()
            print("hubs_arr ", hubs_arr)
            for hub, hub_crd in hubs_arr.items():
                if  hub_crd:
                    print("check hints for  hub: ",hub , " hub coordinates: ", hub_crd)
                    not_exists = self.verify_no_hints_on_hub(hub_crd, config.hubs_hints[hub])
                    if not not_exists:
                        flag = False
                    assert not_exists, "Hints were not disabled on hub: {}".format(hub)
                else:
                    continue
            return flag
        except Exception as e:
            print(e)
            return False

    def verify_full_screen(self,behave=False):
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3],behave)
            sleep(3)
            is_full_screen = self.adb.check_if_device_in_full_screen()
            return is_full_screen
        except Exception as e:
            print(e)

    def notification_selection_all(self):
        for member in self.device_obj(className="android.widget.CheckedTextView"):
            if member.text in " All " and not member.checked:
                member.click()
        sleep(2)
        txt = self.device_obj(resourceId="android:id/alertTitle").text
        if txt in " Allow this app to access Notifications ":
            return True
        return False

    def enable_app_to_access_notifications(self):
        btn = device_obj(resourceId="android:id/button1")
        point = self.canvas.verify_action(self.home_path / "v_icon.png", large_image="")
        if point:
            btn.click()
        else:
            return False
        sleep(2)
        device_obj(resourceId="android:id/switchWidget").click()
        sleep(2)
        device_obj(resourceId="android:id/button1").click()

    def notification_starter_appear_when_screen_off_and_on(self):
        self.screen_off()
        self.screen_on()
        sleep(1)
        point = self.canvas.verify_action(self.home_path / "bell_icon.png", large_image="")
        if point:
            return True
        return False

    def missed_events_comparison(self):
        width, height = self.adb.get_physical_device_screen_size()
        self.device_obj.swipe(width / 2, 10, width / 2, 2000)
        sleep(2)
        # arr = []
        while True:
            for item in self.device_obj(resourceId="android:id/status_bar_latest_event_content"):
                try:
                    print(item.child(resourceId="android:id/title").text)
                except JsonRPCError:
                    self.device_obj.swipe(width / 2, int(height * 0.66), width / 2, int(height *
                                                                                        0.33))
                    print(item.child(resourceId="android:id/title").text)
            break

    def enable_disable_advanced_settings_from_home_screen(self, text, to_enable):
        try:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
            self.open_start()
            self.swipe_menu()
            self.click_on_setting_button()
            self.click_on_advanced_settings()
            self.enable_disable_advanced_settings(text=text, to_enable=to_enable)
            return True
            # self.device_obj.press.back()
            # self.device_obj.press.back()
        except Exception as e:
            self.write_internal_error_to_log()
            raise e

    # noinspection PyTypeChecker
    def write_to_file_all_coordinates(self):
        path = str(self.device_folder / "coordinates.txt")
        with open(path, "w") as f:

            print("plus_point#{}#{}".format(self.plus_point[0], self.plus_point[1]), file=f) \
                if self.plus_point else print("no plus_point")

            print("plus_point_last#{}#{}".format(self.plus_point_last[0],
                                                 self.plus_point_last[1]), file=f) \
                if self.plus_point_last else print("no plus_point_last")

            print("mail_point#{}#{}".format(self.mail_point[0], self.mail_point[1]), file=f) \
                if self.mail_point else print("no mail_point")

            print("sms_point#{}#{}".format(self.sms_point[0], self.sms_point[1]), file=f) \
                if self.sms_point else print("no sms_point")

            print("camera_point#{}#{}".format(self.camera_point[0], self.camera_point[1]),
                  file=f) if self.camera_point else print("no camera_point")

            print("gallery_point#{}#{}".format(self.gallery_point[0], self.gallery_point[1]),
                  file=f) if self.gallery_point else print("no gallery_point")

    def initialize_coordinates_from_file(self):
        content = self.get_coordinates_from_file()
        for item in content:
            if "None" not in item and len(item.split("#")) == 3:
                name, x, y = item.split("#")
                print("Debug Points ", name, ": ", item)
                if name == "plus_point":
                    self.plus_point = (int(x), int(y))
                    self.chrome_point = self.plus_point
                if name == "plus_point_last":
                    self.plus_point_last = (int(x), int(y))
                elif name == "mail_point":
                    self.mail_point = (int(x), int(y))
                elif name == "sms_point":
                    self.sms_point = (int(x), int(y))
                elif name == "camera_point":
                    self.camera_point = (int(x), int(y))
                elif name == "gallery_point":
                    self.gallery_point = (int(x), int(y))

                else:
                    print("item {} was not initialized check initialize_coordinates_from_file "
                          "function".format(name))

    def get_coordinates_from_file(self):
        path = str(self.device_folder / "coordinates.txt")
        with open(path) as f:
            content = f.readlines()
        content = [x.strip() for x in content]
        return content

    def verify_coordinate_file_valid(self):
        content = None
        if os.path.isfile(str(self.device_folder / "coordinates.txt")):
            content = self.get_coordinates_from_file()
        if content:
            for item in content:
                if "None" in item or len(item.split("#")) != 3:
                    print("In verify_coordinate_file_valid , "
                          "coordinates.txt file contains not valid coordinates")
                    return False
            return True
        print("In verify_coordinate_file_valid , coordinates file exists but is empty")
        return False

    @staticmethod
    def write_internal_error_to_log(msg=None):
        if os.environ.get("RAV_INTERNAL_ERROR") == "0":
            os.environ["RAV_INTERNAL_ERROR"] = "1"
            if not msg:
                msg = "     Internal error occured, pleased contact Automation team"
        print(msg)
            # log_html.h_log(msg)
            # log_utilities.print_paths_on_test_failure()

    def setup_your_wallpaper(self):
        frame = config.setup_wallpaper_page_elements.level1_frame
        list = config.setup_wallpaper_page_elements.level2_list
        list_element = config.setup_wallpaper_page_elements.level3_relative
        for view in self.device_obj(className=frame).child(className=list).child(className=list_element):
            view.click()
            os.environ["RAV_BLACK_WALLPAPPER"] = 0
            return True
        return False

    def verify_theme_changed(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        img1 = Path(os.environ.get('HOME_DIRECTORY')) / device_folder_name /  \
               "InitialTheme.png"
        img2 = os.environ.get('HOME_DIRECTORY') + "/" + device_folder_name + "/" + "/Screenshot2.png"
        home_dir = Path(os.environ.get('HOME_DIRECTORY'))
        img3 = home_dir / device_folder_name / "/Screenshot2.png"
        print("img2: ", img2)
        print("img3: ", img3)
        self.adb.screenshot_by_adb(img2)
        result = self.canvas.compare_images(img1, img2)
        print("result by compare_images: ", result)
        if result == "0":
            return True
        else:
            return False

    def click_on_first_theme_in_menu_screen(self):
        print("in click_on_first_theme_in_menu_screen")
        x = int(self.screen_width * 0.3)
        y = int(self.screen_height * 0.1)
        print("clicking on x {}, y {}".format(x,y))
        self.device_obj.click(x,y)


    def verify_text_in_bounds(self, from_x, from_y, to_x, to_y , variants):
        sleep(3)
        img = cv2.imread(str(self.device_folder / "Screenshot.png"))
        crop_img = img[int(from_y * self.screen_height):int(self.screen_height * to_y),
                   int(from_x * self.screen_width):int(to_x * self.screen_width)]
        # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
        new_image = str(self.device_folder / "cropped_wallpapers_screen.png")
        cv2.imwrite(new_image, crop_img)
        result = pytesseract.image_to_string(
            Image.open(new_image))
        result = result.split("\n")
        for line in result:
            for word in line.split(" "):
                if word in variants:
                    return True
        return False

    def click_on_plus_for_more_themes(self):
        print("in click_on_plus_for_more_themes")
        try:
            device_obj(className=config.more_wallpapers.framw_1).child(
            className=config.more_wallpapers.listv_2).child(
            className=config.more_wallpapers.relative_3).child(
            className=config.more_wallpapers.linear_4).child(
            className=config.more_wallpapers.textv_5).click()
            sleep(8)
            return True
        except Exception as e:
            print (e)
            return False

    def click_on_new_theme(self):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        device_obj.click(int(self.screen_width * 0.3), int(self.screen_height * 0.8))
        os.environ["RAV_BLACK_WALLPAPPER"] = "0"
        return self.verify_device_home_screen_display(unlock_status=0,ratio=0.15)

    def set_black_wallpapper(self):
        self.back(3)
        self.swipe_screen()
        self.click_on_setting_button()
        self.select_item_in_settings_by_text(config.Settings_dict.setup_wallpaper)
        try:
            self.scroll_and_click_by_text(config.black_theme_name,False)
        except Exception as e:
            print(e)
            print("second try with scrollable=True ")
            self.scroll_and_click_by_text(config.black_theme_name)
        os.environ["RAV_BLACK_WALLPAPPER"] = "1"

    def get_screen_title(self):
        return self.device_obj(className="android.widget.FrameLayout").child(
            className="android.widget.TextView").text

    def deny_permission(self,logging=False):
        if logging:
            log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        # self.adb.uninstall_and_install_for_displayed_permission()
        # self.open_start()
        # try:
        #     sleep(2)
        #     self.device_obj(resourceId="com.celltick.lockscreen:id/tutorial_1_skip_label").click()
        # except JsonRPCError:
        #     log_html.h_log("faled to click on Skip in Tutorilal screen")
        #     print("Failed to reach to tutorial first skip page after install start")
        #     return False
        try:
            for i in range(6):
                sleep(1)
                device_obj(
                    resourceId="com.android.packageinstaller:id/permission_deny_button").click()
            sleep(3)
            if len(device_obj(resourceId="com.android.packageinstaller:id/permission_deny_button")) > 0:
                device_obj(resourceId="com.android.packageinstaller:id/permission_deny_button").click()
                sleep(2)
                device_obj.press.back()
                device_obj.press.back()
                device_obj.press.back()
            return True
        except JsonRPCError:
            print("Failed to deny permission")
            return False

    def verify_clock_on_or_off(self,img1):
        log_html.decision_logging(inspect.stack()[1], inspect.stack()[0][3], inspect.stack()[1][3])
        self.back(2)
        sleep(1)
        img2 = str(Path(self.device_folder) / "ClockScreenShotAfter.png")
        self.adb.screenshot_by_adb(Path(self.device_folder) / "ClockScreenShotAfter.png")
        status = self.canvas.compare_images(img1,img2)
        if status == "1" or not status:
            return False
        else:
            return True

    def get_to_start_home_screen(self):
        self.back(4)

    def click_on_main_ring(self):
        print("in click on ring")
        if not self.lock_point:
            self.lock_point = self.guess_lock_point()
        self.device_obj.click(self.lock_point[0],self.lock_point[1])

    @staticmethod
    def write_session_header():
        print("in write_session_header")
        log_html.h_log("")
        log_html.h_log("")
        log_html.h_log("*" * 80)
        log_html.h_log("Tests executed on device: {} , id: {} ".format(os.environ[
                                                                           'device_running_name'],
                                                                       os.environ[
                                                                           'device_running_id']),
                       style=log_utilities.html_names.title_test_device)
        log_html.h_log("Apk Version: {}".format(adb_functions.get_apk()))
        print("Debug: 1")
        log_html.h_log("Android Version: {}".format(adb_functions.get_device_android_version()))
        print("Debug: 2")
        log_html.h_log("API Level {}".format(adb_functions.get_device_api_level()))
        print("Debug: 3")
        log_html.h_log("*" * 80)
        log_html.h_log("")

    def try_to_set_black_background_via_more_themes(self):
        print("in try_to_set_black_background_via_more_themes")
        self.swipe_screen_down_up()
        # img_path = Path(os.environ.get("RAV_DEVICE_FOLDER")) / "z_themes_abstract.png"
        # self.adb.screenshot_by_adb(img_path)
        start_index = 10
        end_index = 310
        dict_1, to_scroll = get_bounds(start_index, end_index)
        for i in range(start_index, end_index):
            try:
                if is_black_theme(dict_1[str(i)][1]):
                    dict_1[str(i)][2].click()
                    return True
            except Exception as e:
                print("error at set_black_background , ind {} ".format(i))
                print(e)
                return False

    def swipe_screen_down_up(self):
        height = 1920
        width = 1080
        sx = int(width / 2)
        sy = int(height * 0.95)
        ex = sx
        ey = int(height * 0.1)
        device_obj.swipe(sx, sy, ex, ey)
        #sleep(3)

    def guess_lock_point(self):
        print("in guess_lock_point")
        x = int(self.screen_width/2)
        y = int(self.screen_height*1660/1920)
        return x, y

    def guess_multimedia_hub_point(self):
        print("in guess_multimedia_hub_point")
        x = int(self.screen_width*160/1080)
        if self.lock_point:
            print("lock point exist")
            y = self.lock_point[1]
        else:
            y = int(self.screen_height*1660/1920)
        return x, y

    def guess_unlock_point(self):
        print("in guess_unlock_point")
        x = int(self.screen_width*920/1080)
        if self.lock_point:
            print("lock point exist")
            y = self.lock_point[1]
        else:
            y = int(self.screen_height*1660/1920)
        return x, y
