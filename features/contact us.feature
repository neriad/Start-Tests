# Created by itzik at 3/15/18
Feature: Contact Us
  # Enter feature description here

  Scenario: open contact us
    Given open start
    Then stop recording and save by name "before_open_contact_us"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And clicking on "Contact Us" in settings screen
    Then verify contact us screen contains 3 options
