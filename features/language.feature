# Created by itzik at 5/21/18
Feature: Languages
  # Currently supported languages:
  #  "Hebrew","Spanish","German","Russian","French","Italian","Arabic","Japanese","English","Portuguese","Chinese":

  # Enter feature description here

  Scenario: Change device language
    Given set device language to English
    And wait for "3" seconds
    And open start
    And wait for "1" seconds
    Then verify "English" language
    When exit start by swiping to unlock hub
    And set device language to "Hebrew"
    And open start
    Then verify "Hebrew" language


  Scenario: Change Start Language
    Given open start
    And go to languages screen
    Then verify in languages screen
    And change Start language to "Spanish"
    When clicking on back button "2" times
    Then verify "Spanish" language


  Scenario: Change language on Start and device
    Given set device language to English
    Given open start
    And go to languages screen
    Then verify in languages screen
    And change Start language to "Hebrew"
    When clicking on back button "2" times
    Then verify "Hebrew" language
    When clicking on back button "2" times
    Then unlock device
    And set device language to "French"
    And open start
    Then verify "Hebrew" language


 Scenario: Test1
   Given open start
   And swipe left without verification
   And clicking on settings
   And scroll and click on item named: "Notifications"









