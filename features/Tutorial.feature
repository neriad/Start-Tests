# Created by itzik at 3/15/18
Feature: Tutorial
  # Enter feature description here

  Scenario: open tutorial
    Given open start
    Then stop recording and save by name "before_open_tutorial"
    And wait for "5" seconds
    Then start video recording
    When swipe left
    And clicking on settings
    And clicking on "Tutorial" in settings screen
    Then verify tutorial opened and has all the screens

